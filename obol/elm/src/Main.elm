port module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E
import Parser as P exposing ((|=), (|.))
import String exposing (words)



-- MAIN


main : Program () Model Msg
main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



-- PORTS


port sendMessage : String -> Cmd msg
port messageReceiver : (String -> msg) -> Sub msg



-- MODEL


type LogItem
  = LogText String
  | LogErr String
  | LogSaid String String
  | LogList String (List LogItem)



type alias Model =
  { log : List LogItem
  , cmd : String
  }


init : () -> (Model, Cmd Msg)
init flags =
  ( Model [] ""
  , Cmd.none
  )

logAppend : Model -> LogItem -> Model
logAppend model item  =
  { model | log = model.log ++ [item] }



-- UPDATE


type Msg
  = ReceivedCmd String
  | SetCmd String
  | EnterCmd


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    ReceivedCmd m ->
      case D.decodeString fromServDecoder m of
        Ok fs ->
          handleFromServ fs model
        Err _ ->
          (model, Cmd.none)

    SetCmd cmd ->
      ( { model | cmd = cmd }
      , Cmd.none
      )

    EnterCmd ->
      ( { model | cmd = "" }
      , model.cmd |> getServerCmd |> encodeServerCmd |> sendMessage
      )

handleFromServ : FromServ -> Model -> (Model, Cmd Msg)
handleFromServ fs model =
  case fs of
    Log l ->
      ( logAppend model l, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
  messageReceiver ReceivedCmd



-- VIEW


view : Model -> Html Msg
view model =
  div [id "app"]
    [ div [id "log"] (List.map viewLogItem model.log)
    , Html.form [id "cmd-form", onSubmit EnterCmd]
      [ input
        [ id "cmd-input"
        , type_ "text"
        , value model.cmd
        , autocomplete False
        , spellcheck False
        , onInput SetCmd
        ]
        []
      ]
    ]

viewLogItem : LogItem -> Html Msg
viewLogItem item =
  case item of
    LogText str ->
      span [] [text str]

    LogErr str ->
      span [style "color" "red"]
        [ b [] [text "ERROR: "]
        , text str
        ]

    LogSaid user str ->
      span []
        [ b [] [text <| user ++ ": "]
        , text str
        ]

    LogList header items ->
      span []
        [ b [] [text header]
        , ul [] <| List.map (\x -> li [] [viewLogItem x]) items
        ]




-- MESSAGE FROM SERVER


type FromServ
  = Log LogItem

fromServDecoder : D.Decoder FromServ
fromServDecoder =
  D.field "type" D.string |> D.andThen fromServTypeDecoder

fromServTypeDecoder : String -> D.Decoder FromServ
fromServTypeDecoder t =
  case t of
    "log" ->
      D.map Log (D.field "args" logItemDecoder)

    _ ->
      D.fail <| "Unknown message type: " ++ t

logItemDecoder : D.Decoder LogItem
logItemDecoder =
  D.field "type" D.string |> D.andThen logItemTypeDecoder

logItemTypeDecoder : String -> D.Decoder LogItem
logItemTypeDecoder t =
  case t of
    "text" ->
      D.map LogText <| D.field "val" D.string

    "error" ->
      D.map LogErr <| D.field "val" D.string

    "said" ->
      D.map2 LogSaid
        (D.field "from" D.string)
        (D.field "msg" D.string)

    "list" ->
      D.map2 LogList
        (D.field "header" D.string)
        (D.field "items" <| D.list logItemDecoder)

    _ ->
      D.fail <| "Unknown log type: " ++ t



-- MESSAGE TO SERVER


type ServerCmdName
  = Register
  | Unregister
  | Login
  | Logout
  | Say

type alias ServerCmd =
  { name : ServerCmdName
  , args : List String
  }

serverCmdParser : P.Parser ServerCmd
serverCmdParser =
  P.succeed identity
    |. P.spaces
    |. P.symbol "/"
    |= P.oneOf
      [ slashCmd "register" Register
      , slashCmd "unregister" Unregister
      , slashCmd "login" Login
      , slashCmd "logout" Logout
      ]

slashCmd : String -> ServerCmdName -> P.Parser ServerCmd
slashCmd cmd name =
  P.succeed (ServerCmd name << words)
    |. P.keyword cmd
    |. P.spaces
    |= P.getChompedString (P.chompUntilEndOr "\n")

getServerCmd : String -> ServerCmd
getServerCmd str =
  case P.run serverCmdParser str of
    Ok cmd ->
      cmd

    Err _ ->
      ServerCmd Say [str]

encodeServerCmd : ServerCmd -> String
encodeServerCmd cmd =
  E.encode 0 <| E.object
    [ ("type", E.string <| encodeServerCmdType cmd.name)
    , ("cmd", E.string <| encodeServerCmdName cmd.name)
    , ("args", E.list E.string cmd.args)
    ]


encodeServerCmdType : ServerCmdName -> String
encodeServerCmdType name =
  case name of
    Register ->
      "stranger"
    Unregister ->
      "stranger"
    Login ->
      "stranger"
    Logout ->
      "ghost"
    Say ->
      "vessel"

encodeServerCmdName : ServerCmdName -> String
encodeServerCmdName name =
  case name of
    Register ->
      "register"
    Unregister ->
      "unregister"
    Login ->
      "login"
    Logout ->
      "logout"
    Say ->
      "say"
