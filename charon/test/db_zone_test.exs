defmodule Test.DB.Zone do
  use ExUnit.Case

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Charon.DB.Repo)
  end

  test "Insert a new zone" do
    zone = %Charon.DB.Zone{ name: "Glimmer", visibility: :public }
    zone = Charon.DB.Repo.insert!(zone)
    assert zone.id != nil
  end

  test "Retrieve an inserted zone" do
    zone = %Charon.DB.Zone{ name: "Colossus", visibility: :public }
    zone = Charon.DB.Repo.insert!(zone)
    zone2 = Charon.DB.Repo.get(Charon.DB.Zone, zone.id)
    assert zone == zone2
  end

end
