defmodule Test.DB.Object do
  use ExUnit.Case

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Charon.DB.Repo)
  end

  test "Insert a new object" do
    object = %Charon.DB.Object{ name: "Clam", prog: "", visibility: :public }
    object = Charon.DB.Repo.insert!(object)
    assert object.id != nil
  end

  test "Retrieve an inserted object" do
    object = %Charon.DB.Object{ name: "Oyster", prog: "", visibility: :public }
    object = Charon.DB.Repo.insert!(object)
    object2 = Charon.DB.Repo.get(Charon.DB.Object, object.id)
    assert object == object2
  end

end
