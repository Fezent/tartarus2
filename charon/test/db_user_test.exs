defmodule Test.DB.User do
  use ExUnit.Case

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Charon.DB.Repo)
  end

  test "Insert a new user" do
    user = %Charon.DB.User{ name: "Gregor", password: "beetleboy" }
    user = Charon.DB.Repo.insert!(user)
    assert user.id != nil
  end

  test "Retrieve an inserted user" do
    user = %Charon.DB.User{ name: "Herr. K", password: "schloss" }
    user = Charon.DB.Repo.insert!(user)
    user2 = Charon.DB.Repo.get(Charon.DB.User, user.id)
    assert user == user2
  end

end
