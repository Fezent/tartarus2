
defmodule Charon.Server do
  @moduledoc """
  A process to start and stop the Cowboy server.
  """

  @cowboy_port 8080 # port to start cowboy on

  use GenServer


  ### API

  @doc "Start & link the server process."
  def start_link(_opts), do: GenServer.start_link(__MODULE__, {}, name: __MODULE__)



  ### GenServer Callbacks

  # init process with new cowboy server
  @impl true
  def init(_opts) do
   {:ok, _} = :cowboy.start_clear(
       :cowboy_serv,
       [{:port, @cowboy_port}],
       %{env: %{dispatch: dispatch()}}
   )
   {:ok, {}}
  end

  # shutdown the cowboy server
  @impl true
  def terminate(_reason, _state) do
   :cowboy.stop_listener(:cowboy_serv)
  end



  ### Internal

  # cowboy router
  defp dispatch do
    :cowboy_router.compile([
      {:_, [
        {"/ws", Charon.WebsocketHandler, {}}
      ]}
    ])
  end

end
