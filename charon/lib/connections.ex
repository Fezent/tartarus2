
defmodule Charon.Connections do
  @moduledoc """
  Maintains a table of connected websocket clients.

  Each row of the table contains relevant information about the connection such as:

  - Its status, either: `:stranger` (not logged in), `:ghost` (logged in, but
    not in a zone), or `:player` (logged in, and playing in a zone)
  - Its user ID. For strangers this is `:none`.
  - Its zone. For strangers and ghosts this is `:none`
  - Its location in the zone. For strangers and ghosts this is `:none`
  - Its player object in the zone. For strangers and ghosts this is `:none`

  """

  use GenServer



  ### API

  @doc "Start & link the connections process."
  def start_link(_opts), do: GenServer.start_link(__MODULE__, {}, name: __MODULE__)

  @doc "Add a new connection. (async)"
  def add(c), do: GenServer.cast(__MODULE__, {:add, c})

  @doc "Delete a connection. (async)"
  def del(c), do: GenServer.cast(__MODULE__, {:del, c})

  @doc "Return list of all connections (for debugging). (sync)"
  def dump, do: GenServer.call(__MODULE__, :dump)



  ### GenServer Callbacks

  @impl true
  def init(_opts) do
    conns = :ets.new(:charon_conns_table, [:set, :protected])
    {:ok, conns}
  end

  @impl true
  def terminate(_reason, conns), do: :ets.delete(conns)


  ## Synchronous Messages

  # return all connections
  @impl true
  def handle_call(:dump, _sender, conns) do
    rows = for row <- :ets.match(conns, :'$1'), do: hd(row)
    {:reply, rows, conns}
  end

  # ignore other call messages
  def handle_call(_msg, _sender, conns), do: {:noreply, conns}


  ## Asynchronous Messages

  # add a connection
  @impl true
  def handle_cast({:add, c}, conns) do
    new_conn = {c, :stranger, :none, :none, :none, :none}
    :ets.insert(conns, new_conn)
    {:noreply, conns}
  end

  # remove a connection
  def handle_cast({:del, c}, conns) do
    :ets.delete(conns, c)
    {:noreply, conns}
  end

  # ignore other cast messages
  def handle_cast(_msg, conns), do: {:noreply, conns}

end
