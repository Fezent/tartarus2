defmodule Charon.Application do
  @moduledoc """
  This is the root supervisor for Charon.
  It starts and watches over all the other parts of the application.
  """

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Charon.Server, []},
      {Charon.Connections, []},
      {Charon.DB.Repo, []}
    ]
    opts = [strategy: :one_for_one, name: Charon.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
