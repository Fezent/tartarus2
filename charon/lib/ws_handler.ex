
defmodule Charon.WebsocketHandler do
  @moduledoc """
  A Cowboy websocket handler.

  When a connection is initialized it's registered in the Charon.Connections
  process, and is likewise removed when it's closed.

  This module also exposes the `ws_send(ws, msg)` function to send text back to the client.
  """

  @ws_timeout 15 # time in minutes of inactivity before a websocket is closed



  ### API

  @doc "Send the provided string to the client connected to this websocket. (async)"
  def ws_send(ws, msg) when is_binary(msg), do: send(ws, {:send_msg, msg})
  def ws_send(_ws, _msg), do: raise "Websocket message must be a binary."



  ### Cowboy Websocket Handler Callbacks

  # upgrade http connection to websocket
  def init(req, state) do
    opts = %{idle_timeout: :timer.minutes(@ws_timeout)}
    {:cowboy_websocket, req, {}, opts}
  end

  # init websocket handler
  def websocket_init(state) do
    Charon.Connections.add(self())
    {:ok, state}
  end

  # handle websocket message
  def websocket_handle({:text, raw_msg}, state) do
    {:ok, state}
  end

  # handle other messages
  def websocket_info({:send_msg, msg}, state), do: {[{:text, msg}], state}
  def websocket_info(:close, state), do: {:stop, state}
  def websocket_info(_msg, state), do: {:ok. state}

  # stop handler
  def terminate(_reason, _request, _state) do
    Charon.Connections.del(self())
    :ok
  end

end
