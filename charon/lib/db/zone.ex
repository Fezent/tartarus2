defmodule Charon.DB.Zone do
  use Ecto.Schema

  schema "zones" do
    has_many :objects, Charon.DB.Object
    field :name, :string
    field :visibility, Ecto.Enum, values: [:public, :private]
  end
end
