defmodule Charon.DB.User do
  use Ecto.Schema

  schema "users" do
    belongs_to :object, Charon.DB.Object
    field :name, :string
    field :password, :string
  end
end
