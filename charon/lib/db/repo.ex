defmodule Charon.DB.Repo do
  use Ecto.Repo,
    otp_app: :charon,
    adapter: Ecto.Adapters.Postgres
end
