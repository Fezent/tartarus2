defmodule Charon.DB.Object do
  use Ecto.Schema

  schema "objects" do
    belongs_to :zone, Charon.DB.Zone
    belongs_to :parent, Charon.DB.Object
    has_one :user, Charon.DB.User
    has_many :children, Charon.DB.Object
    field :name, :string
    field :prog, :string
    field :visibility, Ecto.Enum, values: [:public, :private]
  end
end
