import Config

config :charon, Charon.DB.Repo, url: "ecto://postgres:postgres@db/postgres"

config :charon, ecto_repos: [Charon.DB.Repo]
