import Config

config :charon, Charon.DB.Repo,
  url: "ecto://postgres:postgres@db/postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :charon, ecto_repos: [Charon.DB.Repo]

config :logger,
  backends: [:console],
  compile_time_purge_level: :debug
