
CREATE TABLE zones (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	visibility TEXT CHECK (visibility IN ('public', 'private'))
);

CREATE TABLE objects (
	id SERIAL PRIMARY KEY,
	zone_id INTEGER REFERENCES zones,
	parent_id INTEGER REFERENCES objects,
	name TEXT NOT NULL,
	prog TEXT NOT NULL,
	visibility TEXT CHECK (visibility IN ('public', 'private'))
);

CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	object_id INTEGER REFERENCES objects,
	name TEXT NOT NULL,
	password TEXT NOT NULL
);
