# Tartarus II

Tartarus II is an in-progress reimplementation and extension of the original Tartarus system.
It aims to surpass its predecessor in regards to stability, performance, and features.
Started in the summer of 2022, Tartarus II is still in the early stages of development,
but hopefully will soon become the preferred way of running a Tartarus server.


## Tech-Stack

Tartarus II’s back-end (Charon) is written in Elixir (originally Erlang).
The choice to use a BEAM language was motivated by my observation of the inherent concurrency of the platform
(many objects/players/scripts all interacting at the same time)
and the ease with which this runtime facilitates such a model.
The need to handle many simultaneous Websocket connections is also supported by the excellent Cowboy HTTP server.

The front-end for Tartarus II (Obol) is written in Elm, and seeks to be a smarter interface than the older Javascript one.
This enables more things, like command parsing, to be handled by the client-side.

Instead of the janky JSON serialized worlds the original Tartarus utilized,
the new platform stores application state in a Postgres database,
increasing both performance, and accessibility of this data.


## Running

Tartarus II uses Docker-Compose to run everything in nice little containers.

This repo even includes a little bash script named `tartarus` to help get things set up.

You can start Tartarus II in development mode with:

    $ ./tartarus dev

An interactive `iex` shell can be connected to a running Tartarus II Charon dev container for debugging purposes with:

    $ ./tartarus shell

Charon's tests can be run with:

    $ ./tartarus testc

And finally, to start Tartarus II in production mode just use:

    $ ./tartarus start

If you pass the `--bg` flag to `./tartarus start` you can also stop the running instance with:

    $ ./tartarus stop
